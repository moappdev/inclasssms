/* eslint-disable no-console */
import User from '../model/user';
import question from '../model/question';
import response from '../model/response';
import message from '../model/message';
import jwt from 'jsonwebtoken';
import surveyResponses from '../model/surveyResponse';
import config from '../config';
let schedule = require('node-schedule');


let uuid= require('uuid/v4');
let pushbots = require('pushbots');
let Pushbots = new pushbots.api({
    id:'5a0b0f3d9b823a24418b457d',
    secret:'752bd0071d46e694291ff91fa385cb98'
});

let flag = 0;

const messageJobs = {};

function push(users){
    console.log(users);
    Pushbots.setMessage('Coach has a new message for you');//sending to (android and ios) platforms by default add optional paramater "0" for iOS, "1" for Android and "2" for Chrome.
    Pushbots.customFields({'article_id':'1234'});
    Pushbots.customNotificationTitle('Healthy Me');
    //console.log(users);
    Pushbots.sendByTags([users]);
    //Pushbots.sendByAlias(users[i]);
    Pushbots.push(function () {
        console.log('pushed');
    })
}

function scheduleNotifications(messageId) {
    //console.log('schedule nots '+messageId)
    console.log(messageId);
    message.find({mid:messageId},(err,mes)=>{
        let mesg = mes[0];
        //console.log(mes);
        //console.log(mesg.remindtype+' schedule remind type');
        if(mesg.remindtype === -1){
            console.log(mesg.message + 'rule set');
            let s = mesg.publishedtime;
            let s1 = s.split('-');
            let s2 = s1[2].split('T');
            let s3 = s2[1].split(':');
            let s4 = s3[2].split('.');
            let date = new Date(s1[0],s1[1],s2[0],s3[0],s3[1],s4[0]);
            messageJobs[mesg.mid] = schedule.scheduleJob(date, function(){
                push(mesg.users)
            });
        }else if(mesg.remindtype === 0){
            console.log(mesg.message + 'rule set');
            let rule = new schedule.RecurrenceRule();
            let s = mesg.remind[0].split(':');
            rule.hour = s[0];
            rule.minute=s[1];
            rule.second=s[2];
            //console.log(rule);
            messageJobs[mesg.mid] = schedule.scheduleJob(rule,function () {
                console.log('pushed');
                push(mesg.users);
            });
        }else if(mesg.remindtype === 1){
            console.log(mesg.message + 'rule set');
            let rule = new schedule.RecurrenceRule();
            let s = mesg.remind[0].split(':');
            rule.minute=s[1];
            messageJobs[mesg.mid] = schedule.scheduleJob(rule,function () {
                push(mesg.users)
            });
        }else if(mesg.remindtype === 2){
            console.log(mesg.message + 'rule set');
            let rule = new schedule.RecurrenceRule();
            let s = mesg.remind[0].split(':');
            rule.hour = s[0];
            rule.minute=s[1];
            rule.second=s[2];
            messageJobs[mesg.mid] = schedule.scheduleJob(rule,function () {
                push(mesg.users)
            });
            let rule1 = new schedule.RecurrenceRule();
            let s1 = mesg.remind[0].split(':');
            rule1.hour = s1[0];
            rule1.minute=s1[1];
            rule1.second=s1[2];
            messageJobs[mesg.mid+' 1'] = schedule.scheduleJob(rule,function () {
                push(mesg.users)
            });
        }
    });
}

let dbhandlers = {
    login(req,res){
        let username = req.body.username;
        let password = req.body.password;
        User.findOne({'username':username},(err,userDetails)=>{
            if(err){
                console.log(err);
            }else if(userDetails){
                if(password===userDetails.password){
                    let tokenGen=jwt.sign({username:userDetails.username,role:userDetails.role},config.secret);
                    console.log(userDetails.role);
                    return res.status(200).json({success:true,status:'Successful',token:tokenGen,username:userDetails.username,name:userDetails.name});
                }else{
                    console.log(User);
                    return res.status(400).json({success:false,status:'You have entered wrong credentials'});
                }
            }else{
                return res.status(400).json({success:false,status:'User doesn\'t exists'})
            }
        })
    },
    adminlogin(req,res){
        let username = req.body.username;
        let password = req.body.password;
        if(username==='admin'){
            if(password==='admin'){
                let tokenGen=jwt.sign({username:'admin',role:'admin'},config.secret);
                res.status(200).json({success:true,status:'Login Successful',token:tokenGen, username : username});
            }else{
                return res.status(400).json({success:false,status:'You have entered wrong credentials'});
            }
        }else{
            return res.status(400).json({success:false,status:'User doesn\'t exists'})
        }
    },
    logout(req,res){
        let logouttokenid = req.query.token;
        try {
            var tokentoberemoved = tokenManager.get(logouttokenid);
            tokentoberemoved.expire();
        }
        catch(nex){
            return res.status(400).json({success:false,status:'User Session not valid'});
        }
        return res.status(400).json({success:false,status:'User Session Expired'});
    },

    validate(req,res){
        let usertokenid = req.header.token|| req.header('token');
        jwt.verify(usertokenid,config.secret,(err,decoded)=>{
            if(err)
                return res.status(400).json({success:false,message:'failed to authenticate'});
            else{

                return res.status(200).json(
                    {
                        success:true,
                        status:'User Session valid',
                        tokenId:usertokenid,
                        userData:decoded
                    }
                );
            }
        });
    },

    allUsers(req,res) {
        User.find({role:'user'},(err, users)=>
        {
            if(err)
            {
                res.status(400).json({success:false,status:'all user data failed'});
            } else
            {
                res.status(200).send(users);
            }
        })
    },
    deleteAllUsers(req,res){
        User.remove({},(err, data)=>{
            if(err){
                res.status(400).json({success:false,status:'all user deletion failed'});
            }
            else{
                res.status(200).json({success:true,status:'all user deleted'});
            }
        })
    },

    deleteAllQuestions(req,res){
        let username = req.header.username|| req.header('username');
        User.findOne({'username':username},(err, user)=>{
            if(err){
                res.status(400).json({success:false,status:'all user deletion failed'});
            }else if(user){
                if(user.role==='admin'){
                    question.remove({},(err,data)=>{
                        if(err){
                            res.status(400).json({success:false,status:'all question deletion failed'});
                        }
                        else{
                            res.status(200).json({success:true,status:'all questions deleted'});
                        }
                    })
                }else{
                    res.status(200).json({success:false,status:'Insufficient permissions'})
                }
            }
        });
    },
    deleteQuestion(req,res){
        let username = req.header.username|| req.header('username');
        let id=req.body.id;
        User.findOne({'username':username},(err, user)=>{
            if(err){
                res.status(400).json({success:false,status:'all user deletion failed'});
            }else if(user){
                if(user.role==='admin'){
                    question.remove({'_id':id},(err,data)=>{
                        if(err){
                            res.status(400).json({success:false,status:'all question deletion failed'});
                        }
                        else{
                            res.status(200).json({success:true,status:'Question deleted'});
                        }
                    })
                }else{
                    res.status(200).json({success:false,status:'Insufficient permissions'});
                }
            }
        });
    },
    addQuestion(req,res){
        let questionText=req.body;
        let token = req.header('token') || req.header.token;
        jwt.verify(token,config.secret,(err,dec)=>{
            if(err){
                res.status(400).json({success:false,status:'Unsuccessful',error:err});
            }else if(dec){
                if(dec.role==='admin'){
                    let que=new question({
                        'options':questionText.options,
                        'text':questionText.text,
                        'section':questionText.section,
                        'qtype':questionText.type
                    });
                    que.save((err,ques)=>{
                        if(err){
                            return res.status(400).json({success:false,status:'Error!! Please Try Again',error:err.toString()});
                        } else if(ques){
                            return res.status(200).json({success:true,status:'Question added successfully',Question:ques});
                        }else{
                            return res.status(400).json({success:false,status:'Error!! Please Try Again',error:'Question not saved to db'});
                        }
                    });
                }else{
                    res.status(400).json({success:false,status:'Only admin can add questions',error:err});
                }
            }else{
                res.status(400).json({success:false,status:'Unsuccessful'});
            }
        });
    },
    addMessage(req,res){
        let token = req.header('token') || req.header.token;

      let messag = req.body.message;
      let isquestion=req.body.isquestion;
      let isreminder=req.body.isreminder;
      let remind =req.body.remind;
      let remindtype =req.body.remindtype;
      jwt.verify(token,config.secret,(err,dec)=>{
          console.log(token+'  '+dec.role);
          if(err){
              return res.status(200).json({success:false,status:'Error!! Try Again',error:err.toString()});
          }else if(dec){
              if(dec.role==='admin'){
                  let msg=new message({
                      'mid':uuid(),
                      'message': messag,
                      'isquestion': isquestion,
                      'isreminder' : isreminder,
                      'remind' : remind,
                      'remindtype': remindtype
                  });
                  console.log(msg);
                  msg.save((err,mesg)=>{
                      if(err){
                          return res.status(200).json({success:false,status:'Error!! Try Again',error:err.toString()});
                      } else if(mesg){
                          return res.status(200).json({success:true,status:'Question added successfully',message:mesg});
                      }else{
                          return res.status(200).json({success:false,status:'Error!! Please Try Again',error:'Message not saved to db'});
                      }
                  });
              }else{
                  return res.status(200).json({success:false,status:'Only admin can add messages'});
              }
          }
      })
      // console.log(req)
      //check if admin the go ahead

    },
    updateMessage(req,res){
        let users=req.body.users;
        let mid = req.body.mid;
        let ispublished = req.body.ispublished;
        let publishedtime = req.body.publishtime;
        message.update({mid: mid}, { $set: { users: users, ispublished, publishedtime }}, (callback) =>{
            console.log(mid+'update message');
            message.find({},(err,msgs)=>{
                if(err){
                    return res.status(200).json({success:false,status:'Error!!!!! Try Again'});
                }else{
                    if(flag===0){
                        flag=1;
                        for (let i = 0, len = msgs.length; i < len; i++) {
                            scheduleNotifications(msgs[i].mid);
                        }
                    }
                    res.status(200).json({success:true,status:'updated successfully'});
                }
            });

        });
    },
    deleteMessage(req,res){
      let id = req.query.mid
      message.remove({'mid':id},(err,data)=>{
          if(err){
              res.status(200).json({success:false,status:'deletion failed',error:err});
          }
          else{
              /*console.log(messageJobs[id]);
              if(messageJobs[id] !== null){
                  messageJobs[id].cancel();
              }*/
              res.status(200).json({success:true,status:'deletion success'});
          }
      })
    },
    getAllMessage(req,res) {
        let token = req.header('token') || req.header.token;
        jwt.verify(token,config.secret,(err,dec)=>{
            if(err){
                res.status(200).json({success:false,status:'Unsuccessful',error:err})
            }else if(dec){
                if(dec.role==='admin'){
                    message.find({},(err,msgs)=>
                    {
                        if(err)
                        {
                            res.status(200).json({success:false,status:'all User data failed'});
                        } else
                        {
                            res.status(200).send(msgs);
                        }
                    });
                }else{
                    res.status(200).json({success:false,status:'Only admin has permissions'})
                }
            }else{
                res.status(200).json({success:false,status:'Unsuccessful'})
            }
        });
    },
    getMessageForAdminByUsername(req,res){
        let usernam=req.query.username;
        let token = req.header('token') || req.header.token;
        jwt.verify(token,config.secret,(err,dec)=>{
            if(dec){
                if(dec.role==='admin'){
                    message.find({ispublished:true,users:usernam},(err,msgs)=>{
                        if(err)
                        {
                            res.status(200).json({success:false,status:'all Message data failed'});
                        } else
                        {
                            //console.log(dec);
                            response.find({username:usernam},(err,resps)=>{
                                if(err)
                                {
                                    res.status(200).json({success:false,status:'all User data failed'});
                                } else
                                {
                                    res.status(200).json({success:true,status:'Success',message:msgs,responses:resps});
                                }
                            })
                        }
                    });
                }else{
                    res.status(200).json({success:false,status:'Only admin has permissions'});
                }
            }else{
                res.status(400).json({success:false,status:'Unsuccessful'})
            }
        });
    },
    getMessageById(req,res){
        let token = req.header('token') || req.header.token;
        jwt.verify(token,config.secret,(err,dec)=>{
            if(dec){
                message.find({ispublished:true,users:dec.username},(err,msgs)=>{
                    if(err)
                    {
                        res.status(400).json({success:false,status:'all User data failed'});
                    } else
                    {
                        console.log(dec);
                        response.find({username:dec.username},(err,resps)=>{
                            if(err)
                            {
                                res.status(200).json({success:false,status:'all User data failed'});
                            } else
                            {
                                res.status(200).json({success:true,status:'Success',messages:msgs,responses:resps});
                            }
                        })
                    }
                });
            }else{
                res.status(400).json({success:false,status:'Unsuccessful'})
            }
        });
    },
    showQuestion(req,res){
        question.find({},(err,ques)=>{
            if(err){
                return res.status(400).json({success:false,status:'Error!! Try Again'});
            }else if(ques){
                return res.status(200).json({success:true,status:'Successful',questions:ques});
            }else{
                return res.status(400).json({success:false,status:'Error!! Try Again'});
            }
        });
    },
    submitResponse(req,res){
        let token = req.header.token||req.header('token');
        let mid=req.body.mid;
        let answer=req.body.answer;
        jwt.verify(token,config.secret,(err,dec)=>{
           if(err){
               return res.status(200).json({success:false,status:'Error!!!!! Try Again'});
           } else if(dec){
               let newResp = new response({
                   'rid':uuid(),
                   'username':dec.username,
                   'qid':mid,
                   'responses':[answer]
               });

               newResp.save((err,resp)=>{
                   if(err){
                       return res.status(200).json({success:false,status:'Error!! Try Again',error:err});
                   }else{
                       return res.status(200).json({success:true,status:'Success',response:resp});
                   }
               });
           }else{
               return res.status(400).json({success:false,status:'Error Try Again'});
           }
        });
    },
    submitAnswers(req,res){
        let token = req.header('token') || req.header.token;
        let resps= req.body;

        jwt.verify(token,config.secret,(err,dec)=>{
            if(err){
                return res.status(200).json({success:false,status:'Error!! Try Again',error:err});
            }else if(dec){
                let newSurvey = new surveyResponses({
                    'username':dec.username,
                    'responses':resps
                });

                newSurvey.save((err,resp)=>{
                    if(err){
                        return res.status(200).json({success:false,status:'Error!! Please Try Again',error:err});
                    }else{
                        return res.status(200).json({success:true,status:'Success',surveyresponse:resp});
                    }
                });
            }else{
                return res.status(200).json({success:false,status:'Error!! Please Try Again'});
            }
        });

    },
    deleteResponse(req,res){
        let username = req.header.username|| req.header('username');
        let id=req.body.id;
        User.findOne({'username':username},(err, user)=>{
            if(err){
                res.status(400).json({success:false,status:'all user deletion failed'});
            }else if(user){
                if(user.role==='admin'){
                    response.remove({'_id':id},(err,data)=>{
                        if(err){
                            res.status(400).json({success:false,status:'all question deletion failed'});
                        }
                        else{
                            res.status(200).json({success:true,status:'Question deleted'});
                        }
                    })
                }else{
                    res.status(200).json({success:false,status:'Insufficient permissions'})
                }
            }
        });
    },
    getResponse(req,res){
        let username = req.header.username|| req.header('username');
        response.find({'username':username},(err,respo)=>{
            if(err){
                return res.status(200).json({success:false,status:'Error!! Please Try Again'});
            }else if(respo){
                if(respo.responses !== null && respo.length>0){
                    return res.status(200).json({success:true,status:'Successful',response:respo});
                }else{

                    return res.status(200).json({success:false,status:'UnSuccessful',response:respo});
                }
            }else{
                return res.status(200).json({success:false,status:'Error!! Please Try Again'});
            }
        });
    },
    responses(req,res){
        let token = req.header.token|| req.header('token');
        jwt.verify(token,config.secret,(err, dec)=>{
            if(err){
                return res.status(200).json({success:false,status:'Error!! Please Try Again'});
            }else if(dec){
                if(dec.role==='admin'){
                    surveyResponses.find({},(err,resps)=>{
                        if(err){
                            return res.status(200).json({success:false,status:'Error!! Please Try Again'});
                        }else if(resps){
                            return res.status(200).json({success:true,status:'Successful',responses:resps});
                        }else{
                            return res.status(200).json({success:false,status:'Error!! Please Try Again'});
                        }
                    });
                }else{
                    return res.status(200).json({success:false,status:'Only admin can get responses'});
                }
            }
        });
    },
    addUser(req,res){
        //let username = req.header.username|| req.header('username');

        let name = req.body.name;
        let username = req.body.username;
        let password = req.body.password;
        let notification=true;
        let token = req.header('token') || req.header.token;


        /*let newUser = new User({
            'name': 'admin',
            'username': 'admin',
            'password': 'admin',
            'role':'admin',
            'notification':false
        });

        newUser.save((err,u)=>{
            console.log(err);
            if(err)
                return res.status(400).json({success:false,status:'User already exists or you have entered invalid data'});
            else
                return res.status(200).json({success:true,status:'Registration Successful',user:u});
        });*/


        jwt.verify(token,config.secret,(err,decoded)=>{
            if(err){
                return res.status(400).json({success:false,status:'Unsuccessful'});
            }else if(decoded){
                if(decoded.role === 'admin'){
                    let newUser = new User({
                        'name': name,
                        'username': username,
                        'password': password,
                        'role':'user',
                        'notification':notification
                    });

                    newUser.save((err,u)=>{
                        console.log(err);
                        if(err)
                            return res.status(400).json({success:false,status:'User already exists or you have entered invalid data'});
                        else
                            return res.status(200).json({success:true,status:'Registration Successful',userDetails:u});
                    });
                }else{
                    return res.status(400).json({success:false,status:'Only admin can add users'});
                }
            }else{
                return res.status(400).json({success:false,status:'Unsuccessful'});
            }
        })
    },
    updateUser(req,res){
        User.findOneAndUpdate({username:'test6'},{$set:{notification:'false'}},function(err, result){
            if(err){
                return res.status(400).json({success:false,status:'User '});
            }else{
                return res.status(400).json({success:false,status:'User already',result:result});
            }
        })
        /*User.findOne({username: 'test8'},(err, doc)=>{
            if(err){
                res.status(400).json({success:false,status:'Unsucessful'});
            }else if(doc){
                var u=doc;
                //u.notification=false;
                User.remove({'username':doc.username},(err,data)=>{
                    console.log(data);
                    u.save((err,u)=>{
                        console.log(err);
                        if(err)
                            return res.status(400).json({success:false,status:'User already exists or you have entered invalid data'});
                        else
                            return res.status(200).json({success:true,status:'Registration Successful',User:u});
                    });
                });

                //res.status(200).json({success:true,status:'Success',doc:doc});
            }else{
                res.status(400).json({success:false,status:'Unsucessful'});
            }
        });*/

        /*let newUser = new User({
            'name': 'test10',
            'username': 'test10',
            'password': 'test10',
            'role':'User',
            'gender':'male'
        });

        newUser.save((err,u)=>{
            console.log(err);
            if(err)
                return res.status(400).json({success:false,status:'User already exists or you have entered invalid data'});
            else
                return res.status(200).json({success:true,status:'Registration Successful',User:u});
        });*/

    },
    getSurveyResponses(req,res){
        let token = req.header('token') || req.header.token;

        jwt.verify(token,config,(err,dec)=>{
            if(err){
                return res.status(200).json({success:false,status:'Error!! Please Try Again'});
            }else if(dec){
                surveyResponses.find({'username':dec.username},(err,resps)=>{
                    if(err){
                        return res.status(200).json({success:false,status:'Error!! Please Try Again'});
                    }else{
                        return res.status(200).json({success:true,status:'Success',surveyresponse:resps});
                    }
                })
            }else{
                return res.status(200).json({success:false,status:'Error!! Please Try Again'});
            }
        });
    }
};

export default dbhandlers;
