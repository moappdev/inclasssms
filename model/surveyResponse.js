import mongoose from 'mongoose';
//import answer from '../model/answer'



const surveyResponses = new mongoose.Schema({
    'username': {
        'type': String, 'required': true,'unique':true
    },
    'responses': {
        'type':[String]
    }
});



export default mongoose.model('SurveyResponses2', surveyResponses);