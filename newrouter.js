import express from 'express';
let router = express.Router();
import dbhandlers from './services/handler'
import jwt from 'jsonwebtoken';
import config from './config';

export default router;
