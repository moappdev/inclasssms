var webpack = require('webpack');
var WebpackDevServer = require('webpack-dev-server');
var config = require('./webpack.config');
var express =  require('express');
var app = express();
import bodyParser from 'body-parser';
import tweetservice from './services/tweetservice'
import webServices from './services/webservices'
import mongoose from 'mongoose';
import confi from './config.js';
import expressJWT from 'express-jwt';
import jwt from 'jsonwebtoken';
// import router from './routes';
import sms from './sms'


app.use(bodyParser.urlencoded({extended: false}));

app.get('/api/sms',(req,res)=>{
  res.send("in get")
})

app.post('/api/sms',sms.newsms)

app.listen(3000, '0.0.0.0', function (err, result) {
    if (err) {
      console.log(err);
    }
    console.log('Running at http://0.0.0.0:5000');
  });
